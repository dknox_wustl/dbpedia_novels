# Novels in [DBPedia](http://wiki.dbpedia.org/)

[Novels in DBPedia](http://nbviewer.jupyter.org/url/bitbucket.org/dknox_wustl/dbpedia_novels/raw/master/Novels%20in%20DBPedia.ipynb) is a Python Jupyter notebook that addresses three questions:

1) What are all the DBPedia categories for which "novel" is a broader category?

2) Which DBPedia items have one of those categories as a subject?

3) Can we get a plausible list of novels from this?

The notebook walks through an initial exploration focused on finding novels, and then a second run that learns from that and gets some additional information about each novel. The notebook produces a few tab-separated files, and the repository
includes instances from one run of the notebook:

* [`novel_category_names.txt`](./novel_category_names.txt)

* [`item_category_novel_subsets_full.tsv`](./item_category_novel_subsets_full.tsv)  (includes authors and films) 

* [`item_category_novel_subsets_strict.tsv`](./item_category_novel_subsets_strict.tsv) (more successfully limited to novels)

* [`novel_list.tsv`](./novel_list.tsv) (A table that includes author, publication date, language, and author's VIAF id where available.)

This is a minimal start. Much more could be done along these lines.

The notebook depends on [SPARQLWrapper](https://github.com/RDFLib/sparqlwrapper) and [Pandas](https://pandas.pydata.org/), which can be installed with:

```bash
pip install sparqlwrapper
pip install pandas
```

Or, if you are using the [Anaconda](https://www.anaconda.com/download/) distribution:

```bash
conda install -c conda-forge sparqlwrapper
conda install pandas
```
